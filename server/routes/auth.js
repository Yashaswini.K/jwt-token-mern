const router = require("express").Router();
const { User } = require("../models/user");
const Joi = require("joi");
const bcrypt = require("bcrypt");
const Token = require("../models/token");
const sendEmail = require("../utils/sendEmail");
const crypto = require("crypto");

router.post("/", async (req, res) => {
  try {
    console.log("ENTERED", req.body);
    const { error } = validate(req.body);
    console.log(error, "error");
    if (error) {
      return res.status(400).send({ message: error.details[0].message });
    }

    const user = await User.findOne({ email: req.body.email });

    console.log(user, "user");

    if (!user) {
      return res.status(401).send({ message: "Invalid Email or Password" });
    }

    const validPassword = await bcrypt.compare(
      req.body.password,
      user.password
    );
    console.log(validPassword, "validPassword");

    if (!validPassword) {
      return res.status(401).send({ message: "Invalid Email or Password " });
    }

    if (!user.verified) {
      let token = await Token.findOne({ userId: user._id });
      if (!token) {
        token = await new Token({
          userId: user._id,
          token: crypto.randomBytes(32).toString("hex"),
        }).save();

        const url = `${process.env.BASE_URL}users/${user._id}/verify/${token.token}`;

        await sendEmail(user.email, "Verify Email", url);
      }
      return res
        .status(400)
        .send({ message: " An email sent to your account please verify" });
    }

    const token = user.generateAuthToken();
    console.log(token, "token");
    res.status(200).send({ data: token, message: "Logged successfully" });
    // .then(() => console.log("Success"))
    // .catch((err) => console.log(err));
  } catch (error) {
    console.log(error);
    res.status(500).send({ message: "Internal Server Error LOGIn" });
  }
});

const validate = (data) => {
  const schema = Joi.object({
    email: Joi.string().email().required().label("Email"),
    password: Joi.string().required().label("Password"),
  });
  console.log(schema, "schema");
  return schema.validate(data);
};

module.exports = router;
